# motoko-xxhash

xxHash is an extremely fast non-cryptographic hash algorithm. The purpose of this project is to propose an implementation in Motoko for [Internet Computer](https://internetcomputer.org/).

The current reference implementation and the entry point is here  [Yann Collet Cyan4973](https://github.com/Cyan4973/xxHash) 

Additional information may be found in the [Wiki](https://gitlab.com/kurdy/motoko-xxhash/-/wikis/home)

## Motivation and specificity 

A Motoko library to create Hash with the following characteristics:

* Have an alternative to hash function of Motoko
* Have an alternative to cryptographic existing lib
* Have an alternative to crc lib [motoko-crc](https://gitlab.com/kurdy/motoko-crc)
* Recognised as being of good [quality](https://github.com/Cyan4973/xxHash#quality) 
* Efficient with small or large volumes
* Can be built in blocks, to allow large volumes to be processed in several calls.
* Does not require hardware support
* Can be easily controlled or generated on other systems    
* Non-cryptographic but with a seed it's a plus

### Fast ?

Keep in mind that the execution is done in a smartcontract, so we can't compare with offchain implementations. 

The results show that compared to Motoko's internal algorithm it is slower and more consuming in instructions, cycles. It is, however, faster than using a sha256. So, if you want to reduce the risk of collisions and easily compare a offChain hash, this is a possible alternative.      

## Install

I don't use vessel, see [packages registry](https://gitlab.com/kurdy/motoko-xxhash/-/packages) or get last version [v0.4.1](https://gitlab.com/kurdy/motoko-xxhash/-/package_files/60105206/download). Add files to your ./src/ project.

## Quick Start

### xxHash32

Copy lib file __./src/xxh32/lib.mo__ in your motoko project

`import XXH32 "../xxh32";`

#### Single Call

> public func hash(data : [Nat8]) : Nat32

`let h = XXH32.hash([1,2,3]);`

> public func textHash(text : Text) : Nat32

`let h = XXH32.textHash("Alice");`

#### Multiple calls

You must use the same instance during the sequence of update [1..n] and finalize. The instance will be reset at the end of the finalize.

> public func update(data : [Nat8]) : Result<(),Text>

> public func finalize(data : [Nat8]) : Result<Nat32,Text>

```
public func test_19B_up_data() : async () {
    let hasher = XXH32.XXH32(0); // Seed is 0
    let ru=hasher.update([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]);
    switch ru {
        case (#ok()) ();
        case (#err(m)) Debug.trap(m);
    };  
    let rh = hasher.finalize([17,18,19]);
    switch rh {
        case (#ok(h)) assert Nat32.equal(h,3738999843);
        case (#err(m)) Debug.trap(m);
    };   
};
```

The test canister contains examples __./src/tests_xxh32/main.mo__

### xxHash64

Copy lib file __./src/xxh64/lib.mo__ in your motoko project

`import XXH64 "../xxh64";`

#### Single Call

> public func hash(data : [Nat8]) : Nat64 

`let h = XXH64.hash([1,2,3]);`

> public func textHash(text : Text) : Nat64

`let h = XXH64.textHash("Alice");`

#### Multiple calls

You must use the same instance during the sequence of update [1..n] and finalize. The instance will be reset at the end of the finalize.

> public func update(data : [Nat8]) : Result<(),Text>

> public func finalize(data : [Nat8]) : Result<Nat64,Text>

For a large volume, it is necessary to divide the data into chunks (lenght divisible by 16). Then use update and finish with finalize

```
public func test_100MB_data() : async () {
    let data : [Nat8] = Array.freeze<Nat8>(Array.init<Nat8>(2_096_000,0));
    let rdata : [Nat8] = Array.freeze<Nat8>(Array.init<Nat8>(1_488_000,0));
    // 47 × 2_096_000 + 1_488_000 = 100_000_000
    let hasher = XXH64.XXH64(0);
    for (i in Iter.range(1,47)) {
        await update_chunk(data,hasher);
    };
    let r = hasher.finalize(rdata);
    switch r {
        case (#ok(h)) assert Nat64.equal(h,0xcff4305839274e00);
        case (#err(m)) Debug.trap(m);
    };   
    };

    private func update_chunk(data : [Nat8], hasher : XXH64.XXH64) : async () {
    let _ = hasher.update(data);
    };
```

The test canister contains examples __./src/tests_xxh64/main.mo__

## Local deploy

### Prerequisites

* [Dfinity SDK](https://smartcontracts.org/docs/quickstart/local-quickstart.html) - dfx current version 0.12.1
* Optional xxhsum [see](https://github.com/Cyan4973/xxHash#packaging-status)


### deploy

```
cd motoko-xxhash
dfx start --background --clean
dfx deploy
```

### Tests

`dfx canister call tests_xxh32 run_all`

See logs

`dfx canister call tests_xxh64 run_all`

See logs

`dfx canister call tests_xxh64 textHash "Bob"`

(11_312_798_818_365_007_379 : nat64)

`dfx canister call tests_xxh64 hash '(vec{0;1;2;3})'`

(18_432_908_232_848_821_278 : nat64)

`dfx canister call tests_xxh32 textHash "Bob"`

(1_126_886_407 : nat32)

### Examples

Uses a script shell and xxhsum

#### Create a test file

`dd if=/dev/urandom of=2MB.blob bs=1MB count=2`

`./tests/check_file.sh ./2MB.blob`

#### Test a big file

Test with random file of 10MB

```
cd ./tests/bigfile/
./check_big_file_64.sh 10
```