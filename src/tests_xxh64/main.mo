/// Unit tests for module xxH64

import XXH64 "../xxh64";
import Nat32 "mo:base/Nat32";
import Nat64 "mo:base/Nat64";
import Nat8 "mo:base/Nat8";
import Nat "mo:base/Nat";
import Text "mo:base/Text";
import Blob "mo:base/Blob";
import Time "mo:base/Time";
import Int "mo:base/Int";
import Debug "mo:base/Debug";
import Iter "mo:base/Iter";
import Option "mo:base/Option";
import Result "mo:base/Result";
import EIC "mo:base/ExperimentalInternetComputer";
import Array "mo:base/Array";

actor {

    type Result<T,E> = Result.Result<T,E>;

    let xxHash = XXH64.XXH64(0);

    // H=$(xxhsum -H64 /dev/null |  cut -d' ' -f1);echo "0x$H"
    public func test_empty_data() : async () {
        Debug.print("test_empty_data");
        let r = xxHash.finalize([]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0xef46db3751d8e999); 
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H"   
    public func test_1B_data() : async () {
        Debug.print("test_1B_data");
        let r = xxHash.finalize([1]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x8a4127811b21e730);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H"   
    public func test_3B_data() : async () {
        Debug.print("test_3B_data");
        let r = xxHash.finalize([1,2,3]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x743e13ee0c4ee5a5);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H" 
    public func test_4B_data() : async () {
        Debug.print("test_4B_data");
        let r = xxHash.finalize([1,2,3,4]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x542620e3a2a92ed1);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H" 
    public func test_8B_data() : async () {
        Debug.print("test_8B_data");
        let r = xxHash.finalize([1,2,3,4,5,6,7,8]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x814c43eb29646e14);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10\\x11\\x12\\x13\\x14\\x15\\x16\\x17\\x18\\x19\\x1a\\x1b\\x1c\\x1d\\x1e\\x1f | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H" 
    public func test_31B_data() : async () {
        Debug.print("test_31B_data");
        let r = xxHash.finalize([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x68bfa490f8752f55);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10\\x11\\x12\\x13\\x14\\x15\\x16\\x17\\x18\\x19\\x1a\\x1b\\x1c\\x1d\\x1e\\x1f\\x20 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H" 
    public func test_32B_data() : async () {
        Debug.print("test_32B_data");
        let r = xxHash.finalize([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x89614b7813c0bd7f);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10\\x11\\x12\\x13\\x14\\x15\\x16\\x17\\x18\\x19\\x1a\\x1b\\x1c\\x1d\\x1e\\x1f\\x20\\x21\\x22\\x23\\x24\\x25\\x26\\x27\\x28\\x29 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H" 
    public func test_41B_data() : async () {
        Debug.print("test_41B_data");
        let r = xxHash.finalize([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x6effa0ad265e5050);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10\\x11\\x12\\x13\\x14\\x15\\x16\\x17\\x18\\x19\\x1a\\x1b\\x1c\\x1d\\x1e\\x1f\\x20\\x21\\x22\\x23\\x24\\x25\\x26\\x27\\x28\\x29 | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H" 
    public func test_41B_up_data() : async () {
        Debug.print("test_41B_up_data");
        let ru = xxHash.update([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]);
        switch ru {
            case (#ok()) ();
            case (#err(m)) Debug.trap(m);
        }; 
        let r = xxHash.finalize([33,34,35,36,37,38,39,40,41]);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0x6effa0ad265e5050);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -n 'abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓' | xxhsum -H64 - |  cut -d' ' -f1);echo "0x$H"
    public func test_string_data() : async () {
        Debug.print("test_string_data");
        let data = Blob.toArray(Text.encodeUtf8("abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓"));  
        let r = xxHash.finalize(data);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0xf7f7785c9668bc34);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // https://asecuritysite.com/encryption/smh_xxhash
    public func test_string_data_seed() : async () {
        Debug.print("test_string_data with a seed");
        let hasher = XXH64.XXH64(1);
        let data = Blob.toArray(Text.encodeUtf8("abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ"));  
        let r = hasher.finalize(data);
        switch r {
            case (#ok(h)) {
                //Debug.print(Nat32.toText(h));
                assert Nat64.equal(h,0xba960b61676b3f69);
            };
            case (#err(m)) Debug.trap(m);
        };   
    };

    // dd if=/dev/zero of=./tests/100MB.blob bs=1MB count=100 
    // xxhsum -H64  ./tests/100MB.blob  
    public func test_100MB_data() : async () {
        Debug.print("test_100MB_data");
        let data : [Nat8] = Array.freeze<Nat8>(Array.init<Nat8>(2_096_000,0));
        let rdata : [Nat8] = Array.freeze<Nat8>(Array.init<Nat8>(1_488_000,0));
        // 47 × 2_096_000 + 1_488_000 = 100_000_000
        let hasher = XXH64.XXH64(0);
        for (i in Iter.range(1,47)) {
            await update_chunk(data,hasher);
        };
        let r = hasher.finalize(rdata);
        switch r {
            case (#ok(h)) assert Nat64.equal(h,0xcff4305839274e00);
            case (#err(m)) Debug.trap(m);
        };   
    };

    private func update_chunk(data : [Nat8], hasher : XXH64.XXH64) : async () {
        let _ = hasher.update(data);
    };

    public func run_all() : async () {
        await test_empty_data();
        await test_1B_data();
        await test_3B_data();
        await test_4B_data();
        await test_8B_data();
        await test_31B_data();
        await test_32B_data();
        await test_41B_data();
        await test_41B_up_data();
        await test_string_data();
        await test_string_data_seed();
    };

    public func update(data : [Nat8]) : async Result<(),Text> {
        Debug.print("call update");
        let ru=xxHash.update(data);
        switch ru {
            case (#ok()) #ok();
            case (#err(m)) #err(m);
        };  
    };

    public query func textHash(text : Text) : async Nat64 {
        Debug.print("call hashText");
        XXH64.textHash(text); 
    };

    public query func hash(data : [Nat8]) : async Nat64 {
        Debug.print("call hash");
        XXH64.hash(data); 
    };

    public query func countInstructionsTest(text : Text) : async Nat64 {
        Debug.print("call countInstructionsTest");
        EIC.countInstructions(func () {let _ = XXH64.textHash(text)});      
    };

    public func finalize(data : [Nat8]) : async Result<Nat64,Text> {
        Debug.print("call finalize");
        let r=xxHash.finalize(data);
        switch r {
            case (#ok(h)) #ok(h);
            case (#err(m)) #err(m);
        };    
    };

    public func check(data : [Nat8], hash : Nat64) : async Result<Bool,Text> {
        Debug.print("call check");
        let r=xxHash.finalize(data);
        switch r {
            case (#ok(h)) #ok((h == hash));
            case (#err(m)) #err(m);
        };    
    };

    public func reset() : async () {
        Debug.print("call reset");
        let r=xxHash.reset();    
    };
};