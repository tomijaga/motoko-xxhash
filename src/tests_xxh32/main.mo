/// Unit tests for module xxH32
import XXH32 "../xxh32";
import Nat32 "mo:base/Nat32";
import Nat8 "mo:base/Nat8";
import Nat "mo:base/Nat";
import Text "mo:base/Text";
import Blob "mo:base/Blob";
import Time "mo:base/Time";
import Int "mo:base/Int";
import Debug "mo:base/Debug";
import Iter "mo:base/Iter";
import Option "mo:base/Option";
import Result "mo:base/Result";
import EIC "mo:base/ExperimentalInternetComputer";
import Array "mo:base/Array";

actor {

    type Result<T,E> = Result.Result<T,E>;

    let xxHash = XXH32.XXH32(0);

    // H=$(xxhsum -H0 /dev/null |  cut -d' ' -f1);echo "0x$H $((16#$H))"
    public func test_empty_data() : async () {
        Debug.print("test_empty_data");
        let r = xxHash.finalize([]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,46947589);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))"   
    public func test_1B_data() : async () {
        Debug.print("test_1B_data");
        let r = xxHash.finalize([1]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,949155633);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))"   
    public func test_3B_data() : async () {
        Debug.print("test_3B_data");
        let r = xxHash.finalize([1,2,3]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,4120672452);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))" 
    public func test_4B_data() : async () {
        Debug.print("test_4B_data");
        let r = xxHash.finalize([1,2,3,4]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,4271296924);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))" 
    public func test_6B_data() : async () {
        Debug.print("test_6B_data");
        let r = xxHash.finalize([1,2,3,4,5,6]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,3005759874);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))" 
    public func test_16B_data() : async () {
        Debug.print("test_16B_data");
        let r = xxHash.finalize([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,4113429260);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10\\x11\\x12\\x13 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))" 
    public func test_19B_data() : async () {
        Debug.print("test_19B_data");
        let r = xxHash.finalize([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,3738999843);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -en \\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0a\\x0b\\x0c\\x0d\\x0e\\x0f\\x10\\x11\\x12\\x13 | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))" 
    public func test_19B_up_data() : async () {
        Debug.print("test_19B_up_data");
        let ru=xxHash.update([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]);
        switch ru {
            case (#ok()) ();
            case (#err(m)) Debug.trap(m);
        };  
        let rh = xxHash.finalize([17,18,19]);
        switch rh {
            case (#ok(h)) assert Nat32.equal(h,3738999843);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // H=$( echo -n 'abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓' | xxhsum -H0 - |  cut -d' ' -f1);echo "0x$H $((16#$H))"
    public func test_string_data() : async () {
        Debug.print("test_string_data");
        let data = Blob.toArray(Text.encodeUtf8("abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓"));  
        let r = xxHash.finalize(data);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,902259818);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // https://asecuritysite.com/encryption/smh_xxhash
    public func test_string_data_seed() : async () {
        Debug.print("test_string_data with a seed");
        let hasher = XXH32.XXH32(1);
        let data = Blob.toArray(Text.encodeUtf8("abcdefghijklmnopqrstuvwxyz"));  
        let r = hasher.finalize(data);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,3403643528);
            case (#err(m)) Debug.trap(m);
        };   
    };

    // dd if=/dev/zero of=./tests/100MB.blob bs=1MB count=100 
    // xxhsum -H32  ./tests/100MB.blob 
    public func test_100MB_data() : async () {
        Debug.print("test_100MB_data");
        let data : [Nat8] = Array.freeze<Nat8>(Array.init<Nat8>(2_096_000,0));
        let rdata : [Nat8] = Array.freeze<Nat8>(Array.init<Nat8>(1_488_000,0));
        // 47 × 2_096_000 + 1_488_000 = 100_000_000
        let hasher = XXH32.XXH32(0);
        for (i in Iter.range(1,47)) {
            await update_chunk(data,hasher);
        };
        let r = hasher.finalize(rdata);
        switch r {
            case (#ok(h)) assert Nat32.equal(h,0xcb9b4a03);
            case (#err(m)) Debug.trap(m);
        };   
    };

    private func update_chunk(data : [Nat8], hasher : XXH32.XXH32) : async () {
        let _ = hasher.update(data);
    };

    public func run_all() : async () {
        await test_empty_data();
        await test_1B_data();
        await test_3B_data();
        await test_4B_data();
        await test_6B_data();
        await test_16B_data();
        await test_19B_data();
        await test_19B_up_data();
        await test_string_data();
        await test_string_data_seed();
    };


    public func update(data : [Nat8]) : async Result<(),Text> {
        Debug.print("call update");
        let ru=xxHash.update(data);
        switch ru {
            case (#ok()) #ok();
            case (#err(m)) #err(m);
        };  
    };


    public query func textHash(text : Text) : async Nat32 {
        Debug.print("call hashText");
        XXH32.textHash(text); 
    };

    public query func hash(data : [Nat8]) : async Nat32 {
        Debug.print("call hash");
        XXH32.hash(data); 
    };

    public query func countInstructionsTest(text : Text,seed : ?Nat32) : async Nat64 {
        Debug.print("call countInstructionsTest");
        EIC.countInstructions(func () {let _ = XXH32.textHash(text)});      
    };

    public func finalize(data : [Nat8]) : async Result<Nat32,Text> {
        Debug.print("call finalize");
        let r=xxHash.finalize(data);
        switch r {
            case (#ok(h)) #ok(h);
            case (#err(m)) #err(m);
        };    
    };

    public func check(data : [Nat8], hash : Nat32) : async Result<Bool,Text> {
        Debug.print("call check");
        let r=xxHash.finalize(data);
        switch r {
            case (#ok(h)) #ok((h == hash));
            case (#err(m)) #err(m);
        };    
    };

    public func reset() : async () {
        Debug.print("call reset");
        let r=xxHash.reset();    
    };
};