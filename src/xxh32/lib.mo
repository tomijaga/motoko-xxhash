/// xxHash32 module implementation
///
/// Specification : https://github.com/Cyan4973/xxHash/blob/dev/doc/xxhash_spec.md#xxh32-algorithm-description
///
/// repo: https://gitlab.com/kurdy/motoko-xxhash
///
/// License: BSD 3-Clause
import Nat32 "mo:base/Nat32";
import Nat8 "mo:base/Nat8";
import Nat "mo:base/Nat";
import Iter "mo:base/Iter";
import Buffer "mo:base/Buffer";
import Array "mo:base/Array";
import Debug "mo:base/Debug";
import Text "mo:base/Text";
import Option "mo:base/Option";
import Result "mo:base/Result";
import Bool "mo:base/Bool";
import Blob "mo:base/Blob";



module {

  type Result<T,E> = Result.Result<T,E>;

  /*
    * These constants are prime numbers, and feature a good mix of bits 1 and 0,
    * neither too regular, nor too dissymmetric. These properties help dispersion capabilities.
    */
  let PRIME32_1 : Nat32 = 0x9E3779B1;  // 0b10011110001101110111100110110001
  let PRIME32_2 : Nat32 = 0x85EBCA77;  // 0b10000101111010111100101001110111
  let PRIME32_3 : Nat32 = 0xC2B2AE3D;  // 0b11000010101100101010111000111101
  let PRIME32_4 : Nat32 = 0x27D4EB2F;  // 0b00100111110101001110101100101111
  let PRIME32_5 : Nat32 = 0x165667B1;  // 0b00010110010101100110011110110001 374_761_393
  let STRIPES_SIZE : Nat = 16;
  let LANE_SIZE : Nat = 4;

  public class XXH32 (seed : Nat32) {
      
    var acc1 : Nat32 = seed +% PRIME32_1 +% PRIME32_2;
    var acc2 : Nat32 = seed +% PRIME32_2;
    var acc3 : Nat32 = seed;
    var acc4 : Nat32 = seed -% PRIME32_1;
    var atLeastOneStripe : Bool = false;
    var dataSize : Nat = 0;

    /// Reset state of instance after interruption of update's or finalize  
    public func reset() {
      acc1 := seed +% PRIME32_1 +% PRIME32_2;
      acc2 := seed +% PRIME32_2;
      acc3 := seed;
      acc4 := seed -% PRIME32_1;
      dataSize := 0;
      atLeastOneStripe := false;
    };
    
    /// Update state with a block of data
    /// The data block must be divisible by 16. The remainder will be passed to the finalize method
    /// Limitation:
    /// * 2_950_000 bytes over that limit you may trap with exceeded the instruction limit for single message execution.
    /// * 2_097_152 bytes which is the maximum size of a parameter call outside of canister.
    /// * update should be call with update func
    /// From specification is __Step 2. Process stripes__
    public func update(data : [Nat8]) : Result<(),Text>{
      let size = Iter.size(Array.vals(data));

      // Test if the data block is at least sized to STRIPES_SIZE 
      if (size < STRIPES_SIZE) {
        return #err("The minimum size of block is " # Nat.toText(STRIPES_SIZE) # " bytes. You may use directly finalize.");
      };
      // Test if the data block is divisible by STRIPES_SIZE
      if(size % STRIPES_SIZE != 0){
        return #err("The size of the data block must be divisible by " # Nat.toText(STRIPES_SIZE) #  ". Use finalize to transmit the last incomplete block. (< " # Nat.toText(STRIPES_SIZE) # " bytes)");
      };

      atLeastOneStripe:= true;
      dataSize += size;
      // compute stripe of data Block 
      var cursor : Nat = 0;
      let lane = Buffer.Buffer<Nat8>(0);
      let stripe = Buffer.Buffer<[Nat8]>(0);
      let nbLanes = STRIPES_SIZE / LANE_SIZE;
      while(cursor < size) {
        let b = data[cursor];
        lane.add(b);
        if (lane.size() >= LANE_SIZE) {
          stripe.add(Buffer.toArray(lane));
          lane.clear();
        };
        if (stripe.size() >= nbLanes) {
          processStripe(Buffer.toArray(stripe));
          stripe.clear();
        };
        cursor+=1;
      };
      return #ok()
    };

    /// Finalize the computation
    /// Limitation:
    /// * 2_950_000 bytes over that limit you may trap with error: exceeded the instruction limit for single message execution.
    /// * 2_097_152 bytes which is the maximum size of a parameter call outside of canister.
    /// If your data is larger than the limits below, you must split it into blocks divisible by 16 
    /// and proceed through update then finalize with the reminding data.
    public func finalize(data : [Nat8]) : Result<Nat32,Text>{
      var hash : Nat32 = 0;
      let size = Iter.size(Array.vals(data));
      var remindData: [Nat8] = [];
      let hasData : Bool = atLeastOneStripe or (size != 0); // atLeastOneStripe=true means update has been called before

      if(hasData) {
        // Test if data size is greater to a stripe size if yes we need to call update before
        if(size >= STRIPES_SIZE) {
          let dataBlock = Buffer.Buffer<Nat8>(0);
          let nbStripes = size / STRIPES_SIZE;
          let nbRemind = size % STRIPES_SIZE;
          let lastByte : Nat = (nbStripes * STRIPES_SIZE) - 1;
          for (i in Iter.range(0,lastByte)) {
            dataBlock.add(data[i]);
          };
          let result = update(Buffer.toArray(dataBlock));
          dataBlock.clear();
          switch result { 
            case (#err(m)) return #err("finalize error: " # m);
            case (#ok) ();
          };
          
          if(nbRemind != 0 ) {
            for (i in Iter.range(lastByte+1,size - 1)) {
              dataBlock.add(data[i]);
            };
            remindData := Buffer.toArray(dataBlock);
            dataBlock.clear();
          };
        } else {
          remindData := data;
        };

        // Accumulator convergence
        // From specification: __Step 3. Accumulator convergence__
        if(atLeastOneStripe) {
          hash := acc1 <<> 1;
          hash := hash +% (acc2 <<> 7);
          hash := hash +% (acc3 <<> 12);
          hash := hash +% (acc4 <<> 18);
        } else {
          hash := seed +% PRIME32_5;
        };

        // Add data size in byte to the hash
        // From specification:  __Step 4. Add input length__
        hash +%= Nat32.fromNat(dataSize + Iter.size(Array.vals(remindData)));

        // From specification:  __Step 5. Consume remaining input__
        hash := processRemindingData(hash,remindData);
      } else {
        // Data is empty
        hash := seed +% PRIME32_5;
      };
      reset();
      // __Step 6. Final mix (avalanche)__
      return #ok(avalanche(hash));
    };

    /*
     * Begin of private methods
     */
    private func avalanche (hash : Nat32) : Nat32 {
      var acc : Nat32 = hash;
      acc ^= acc >> 15;
      acc *%= PRIME32_2;
      acc ^= acc >> 13;
      acc *%= PRIME32_3;
      acc ^= acc >> 16;
      return acc;
    };

    // Step 2
    private func processLane (lane : Nat32, acc : Nat32) : Nat32 {
      var result = acc;
      result +%= (lane *% PRIME32_2);
      result <<>= 13;
      result *%= PRIME32_1;
      return result;
    };

    // Step 2
    private func processStripe (stripe : [[Nat8]]) {
      acc1 := processLane(toNat32(stripe[0]),acc1);
      acc2 := processLane(toNat32(stripe[1]),acc2);
      acc3 := processLane(toNat32(stripe[2]),acc3);
      acc4 := processLane(toNat32(stripe[3]),acc4);
    };

    // Step 5
    private func processRemindingData (hash : Nat32, data: [Nat8]) : Nat32 {
      let size : Nat = Iter.size(Array.vals(data));
      if(size == 0) {
        // Nothing todo
        return hash;
      };

      var result = hash;
      if(size > STRIPES_SIZE){
        Debug.trap("Reminding data should be < " # Nat.toText(STRIPES_SIZE) # " bytes.");
      };
      
      let lane = Buffer.Buffer<Nat8>(0);
      let upRange : Nat = size - 1;
      for (i in Iter.range(0,upRange)) {
        lane.add(data[i]);
        if(lane.size() >= LANE_SIZE) {
          result +%= toNat32(Buffer.toArray(lane)) *% PRIME32_3;
          result := result <<> 17; 
          result *%= PRIME32_4;
          lane.clear();
        };
      };

      if(lane.size() > 0) {
        for (b in lane.vals()){
          let bl = Nat32.fromNat(Nat8.toNat(b));
          result +%= bl *% PRIME32_5;
          result := (result <<> 11) *% PRIME32_1;
        };
      };
      return result;
    };

    private func toNat32 (lane : [Nat8]) : Nat32 {
      var result : Nat32 = 0;
      var shift : Nat32 = 0;
      for (byte in lane.vals()){
        result += Nat32.fromNat(Nat8.toNat(byte)) << shift;
        shift += 8;
      };
      return result;
    };  
  }; // End of class

  /// Quick and easy call for data size below < 2097152 seed 0 and trap if error.
  /// Can be use in query func
  public func hash(data : [Nat8]) : Nat32 {
    let hasher = XXH32(0);
    let r = hasher.finalize(data);
    switch r {
      case (#ok(h)) return h;
      case (#err(m)) Debug.trap(m);
    }; 
  };

  /// Quick and easy call for data size below < 2097152 seed 0 and trap if error.
  /// Can be use in query func
  public func textHash(text : Text) : Nat32 {
    let hasher = XXH32(0);
    let data = Blob.toArray(Text.encodeUtf8(text));  
    let r = hasher.finalize(data);
    switch r {
      case (#ok(h)) return h;
      case (#err(m)) Debug.trap(m);
    };
  };

};