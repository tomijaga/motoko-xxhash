#!/bin/sh
# Allow to check a file with xxH32 lib using vec type parameters
# Command sample ./tests/check_file.sh ./tests/2MB.blob  
# max allowed data size 2097152
# dd if=/dev/urandom of=./tests/MaxSize.blob bs=1B count=2096800   
# Create 2MB file  dd if=/dev/urandom of=2MB.blob bs=1MB count=2

status=$((0)) 

TMP_FILE=$(mktemp)

# Create beginning parameters
echo -n "( vec{" > $TMP_FILE

# Read file and create a string \xx\yy
for c in $(xxd -c 1 -p $1)
do 
  echo -n "0x$c;" >> $TMP_FILE; 
done;

# Close vec {...}
echo -n "}" >> $TMP_FILE;

# H=$(xxhsum -H0 ./tests/1MB.blob |  cut -d' ' -f1);echo "0x$H $((16#$H))" 
H=$(xxhsum -H64 $1 |  cut -d' ' -f1)

# Add hash and close param
#echo -n ",$((16#$H)))"  >> $TMP_FILE;
echo -n ",0x$H)"  >> $TMP_FILE;

# echo "Parameter length: $(stat -c %s $TMP_FILE)"

time dfx canister call --argument-file $TMP_FILE tests_xxh64 check

status=$((status + $?))

rm $TMP_FILE

exit $((status + $?))