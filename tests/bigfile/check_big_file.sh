#!/bin/sh
# Allow to check a file with xxH32 lib using vec type parameters
# 
# Create file  
# dd if=/dev/urandom of=100MB.blob bs=1MB count=100
# split -d -b 2096000 100MB.blob part_100MB.
# xxhsum -H0 100MB.blob > check.hash  
# find part_* -type f | sort

# Create bigFile

# Big File size in MB
FILE_SIZE=$1

if [ -z "$FILE_SIZE" ]; then
    FILE_SIZE=100
fi

dd if=/dev/urandom of="$FILE_SIZE"MB.blob bs=1MB count=$FILE_SIZE
split -d -b 2096000 "$FILE_SIZE"MB.blob part_"$FILE_SIZE"MB.

declare -a FILES=($(find ./ -type f | grep "\.[0-9]*$" | sort))

SIZE=${#FILES[@]}

LOOP=$(($SIZE - 2))

dfx canister call tests_xxh32 reset

status=$((0)) 

for ((i=0;i<=$LOOP;i++)); 
do 
  unset $TMP_FILE 
  echo ${FILES[$i]}
  TMP_FILE=$(mktemp)

  # Create beginning parameters
  echo -n "( vec{" > $TMP_FILE

  # Read file and create a string \xx\yy
  for c in $(xxd -c 1 -p ${FILES[$i]})
  do 
    echo -n "0x$c;" >> $TMP_FILE;  
  done;

  # Close vec {...}
  echo -n "})" >> $TMP_FILE;

  dfx canister call --argument-file $TMP_FILE tests_xxh32 update | grep "ok"
  status=$((status + $?))

  rm $TMP_FILE

  # echo ${FILES[$i]}
done

unset $TMP_FILE
TMP_FILE=$(mktemp)

# Create beginning parameters
echo -n "( vec{" > $TMP_FILE

# Read file and create a string \xx\yy
for c in $(xxd -c 1 -p ${FILES[$(($SIZE - 1))]})
do 
  echo -n "0x$c;" >> $TMP_FILE; 
done;

# Close vec {...}
echo -n "}" >> $TMP_FILE;

H=$(xxhsum -H0 "$FILE_SIZE"MB.blob |  cut -d' ' -f1)

# Add hash and close param
echo -n ",$((16#$H)))"  >> $TMP_FILE;

echo "Hash: 0x$H"

dfx canister call --argument-file $TMP_FILE tests_xxh32 check | grep "ok = true"
status=$((status + $?))

rm $TMP_FILE

rm part_*.??;
rm *.blob;

exit $((status + $?))